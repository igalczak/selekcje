<?php 
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '1');

require '../SQL.php';
$sql = new SQL('192.168.10.13', 'root', 'password', null);


//DB_NAME:WHATEVER NAME => SELECT QUERY

$queries = array(
		'Pools' => array(
			'Adult' => array('query'=>"SELECT email FROM `pools`.`Pool-Adult`"),
			'Biblioteki' => array('query'=>"SELECT email FROM `pools`.`Pool-Library`"),
			'Biznes' =>array('query'=> "SELECT email FROM `pools`.`Pool-Biznes`"),
			'Domy kultury' => array('query'=>"SELECT email FROM `pools`.`Pool-CulturalCenter`"),
			'Drogi' => array('query'=>"SELECT email FROM `pools`.`Pool-Roads`"),
			'Fundraiser' =>array('query'=> "SELECT email FROM `pools`.`Pool-Fundraiser`"),
			'Fundraiser-program' => array('query'=>"SELECT email FROM `pools`.`Pool-Fundraiser-program`"),
			'Geodezja' => array('query'=>"SELECT email FROM `pools`.`Pool-Geodesy`"),
			'Katering' => array('query'=>"SELECT email FROM `pools`.`Pool-Catering`"),
			'Komunalne' =>array('query'=> "SELECT email FROM `pools`.`Pool-Komunalne`"),
			'Leady' => array(
					'ALL' => array('query'=>"SELECT email FROM `pools`.`Pool-Leads`"),
					'EDU' => array('query'=>"SELECT email FROM `pools`.`Pool-Leads` WHERE `market`='EDU'"),
					'Rynek LIKE' => array('query'=>"SELECT email FROM `pools`.`Pool-Leads` WHERE `market` LIKE '%market$%'", 
							'variables'=>array(array('name'=>'market$', 'show_name'=>'Rynek'))),
					'Rynek NOT LIKE' => array('query'=>"SELECT email FROM `pools`.`Pool-Leads` WHERE `market` NOT LIKE '%market$%'",
							'variables'=>array(array('name'=>'market$', 'show_name'=>'Rynek'))),
					'Webinar' => array('query'=>"SELECT email FROM `pools`.`Pool-Leads` WHERE `db-source` LIKE '%webinar%'"),
					'Webinar-2016-12-09' => array('query'=>"SELECT email FROM `pools`.`Pool-Leads` WHERE `db-source` LIKE '%webinar-2016-12-09%'"),
			),
			'Młodzież' => array('query'=>"SELECT email FROM `pools`.`Pool-Young`"),
			'Nauczyciele' => array('query'=>"SELECT email FROM `pools`.`Pool-Teachers`"),
			'NGO' => array('query'=>"SELECT email FROM `pools`.`Pool-NGO`"),
			'PPP' =>array('query'=> "SELECT email FROM `pools`.`Pool-PPP`"),
			'Przedszkola' =>array('query'=> "SELECT email FROM `pools`.`Pool-Kindergarten`"),
			'Publiczne' =>array('query'=> "SELECT email FROM `pools`.`Pool-Public`"),
			'Szkoły' => array('query'=>"SELECT email FROM `pools`.`Pool-School`"),
			'Zamówienia publiczne' => array('query'=>"SELECT email FROM `pools`.`Pool-PublicTenders`"),
			'Zawodowe' => array('query'=>"SELECT email FROM `pools`.`Pool-Vocational`"),
			'Zeas' =>array('query'=> "SELECT email FROM `pools`.`Pool-Zeas`"),
		),
		'MEN' => array(
			'ALL' => array('query'=>"SELECT email FROM `MEN`.`MEN2016-emails`"),
			'Szkoły' => array('query'=>"SELECT `MEN`.`MEN2016-emails`.`email` AS email FROM ( `MEN`.`MEN2016` INNER JOIN `MEN`.`Typy` 
ON `MEN`.`MEN2016`.`typ` = `MEN`.`Typy`.`typ`) INNER JOIN `MEN`.`MEN2016-emails` 
ON `MEN`.`MEN2016`.`id` = `MEN`.`MEN2016-emails`.`id-men2016`
WHERE `MEN`.`Typy`.`szkoly`='-1' AND `MEN`.`MEN2016-emails`.`email` Is Not Null"),
				'Przedszkola' => array('query'=>"SELECT `MEN`.`MEN2016-emails`.`email` AS email FROM ( `MEN`.`MEN2016` INNER JOIN `MEN`.`Typy`
ON `MEN`.`MEN2016`.`typ` = `MEN`.`Typy`.`typ`) INNER JOIN `MEN`.`MEN2016-emails`
ON `MEN`.`MEN2016`.`id` = `MEN`.`MEN2016-emails`.`id-men2016`
WHERE `MEN`.`Typy`.`przedszkola`='-1' AND `MEN`.`MEN2016-emails`.`email` Is Not Null"),
		),
		'JST' => array(
			'ALL (osoby)' => array('query'=>"SELECT email FROM `JST`.`JST_Osoby`"),
			'Wydziały oświaty' => array('query'=>"SELECT email FROM `JST`.`_Selekcja-JST-Wydzialy-oswiaty`"),
			'Bez wydziałów oświaty' => array('query'=>"SELECT email FROM `JST`.`_Selekcja-JST-all-bez-WO`"),
			'Władze JST' => array('query'=>"SELECT email FROM `JST`.`_Selekcja-JST-wladze`"),
			'Drogi (wg wydziału *dr?g* OR *infastr*)'=>array('query'=>"SELECT email FROM `JST`.`JST_Wydzialy` WHERE nazwa LIKE '%dr_g%' OR nazwa LIKE '%infrastr%'"),
			'Bazy gmin' => array('query'=>"SELECT `JST`.`JST_Osoby`.`email` FROM `JST`.`JST_BG` INNER JOIN `JST`.`JST_Osoby`  ON `JST`.`JST_BG`.`id-BazaGmin` = `JST`.`JST_Osoby`.`id-BazaGmin` WHERE `JST`.`JST_BG`.`id-BazaGmin` != 0 AND `JST`.`JST_BG`.`typ` = 'BG'"),
			'Nazwa wydziału LIKE' => array('query'=>"SELECT email FROM `JST`.`JST_Wydzialy` WHERE `nazwa` LIKE '%name$%'",
				'variables'=>array(array('name'=>'name$', 'show_name'=>'Wydział'))),
		),
		'KAPT' => array(
				'ALL' => array('query'=>"SELECT email FROM `KAPT`.`Klienci-emails`"),
				'EDU' => array('query'=>"SELECT email FROM `KAPT`.`Klienci-emails` WHERE `market` LIKE 'edu'"),
				'PSDS - aktywni' => array('query'=>"SELECT email FROM `KAPT`.`Klienci-emails` WHERE `product` LIKE 'psds'  AND (DATE(`resignation_date`) > DATE(NOW()) OR `resignation_date` = '0000-00-00')"),
				'PSDS - rezygnaci' => array('query'=>"SELECT email FROM `KAPT`.`Klienci-emails` WHERE `product` LIKE 'psds' AND DATE(`resignation_date`) < DATE(NOW()) AND resignation_date != '0000-00-00'"),
				'Drogi - aktywni' => array('query'=>"SELECT email FROM `KAPT`.`Klienci-emails` WHERE `product` LIKE '%drogi%'  AND (DATE(`resignation_date`) > DATE(NOW()) OR `resignation_date` = '0000-00-00')"),
				'Drogi - rezygnaci' => array('query'=>"SELECT email FROM `KAPT`.`Klienci-emails` WHERE `product` LIKE '%drogi%' AND DATE(`resignation_date`) < DATE(NOW()) AND resignation_date != '0000-00-00'"),
				'Rynek = {DROGI, GEO, SEMAS}' => array('query'=>"SELECT email FROM `KAPT`.`Klienci-emails` WHERE `market` LIKE '%drogi%' OR `market` LIKE '%geo%' OR `market` LIKE '%semas%'"),
				'Rynek LIKE' => array('query'=>"SELECT email FROM `KAPT`.`Klienci-emails` WHERE `market` LIKE '%market$%'",
					'variables'=>array(array('name'=>'market$', 'show_name'=>'Rynek'))),
				'Produkt LIKE' => array('query'=>"SELECT email FROM `KAPT`.`Klienci-emails` WHERE `product_short_name` LIKE '%product$%'",
						'variables'=>array(array('name'=>'product$', 'show_name'=>'Produkt (short_name)'))),
				'Produkt LIKE AND Data zamówienia > ' => array('query'=>"SELECT email FROM `KAPT`.`Klienci-emails` WHERE DATE(`order_date`)>DATE('date$') AND `product_short_name` LIKE '%product$%'",
					'variables'=>array(array('name'=>'product$', 'show_name'=>'Produkt (short_name)'),array('name'=>'date$', 'show_name'=>'Data zamówienia'))),
				'2 Produkty LIKE AND Data zamówienia > ' => array('query'=>"SELECT email FROM `KAPT`.`Klienci-emails` WHERE DATE(`order_date`)>DATE('date$') AND (`product_short_name` LIKE '%product1$%' OR `product_short_name` LIKE '%product2$%')",
						'variables'=>array(array('name'=>'product1$', 'show_name'=>'Produkt 1(short_name)'),array('name'=>'product2$', 'show_name'=>'Produkt 2(short_name)'),array('name'=>'date$', 'show_name'=>'Data zamówienia'))),
		
		),
		'Selekcje' => array()
		
		
);

$adresy_kontrolne = array(
		'annag@agencjapracytworczej.pl',
		'marcink@agencjapracytworczej.pl',
		'pierszylepszy@gmail.com',
		'nina.kinitz@agencjapracytworczej.pl',
		'maria.winiarska@agencjapracytworczej.pl',
		'magdalena.bobrowicz@agencjapracytworczej.pl',
		'monika.slomka@agencjapracytworczej.pl',
		'igor.galczak@gmail.com',
);

$selekcje = $sql->FetchAll("SHOW TABLES FROM Selekcje");
foreach ($selekcje as $selekcja){
	$queries['Selekcje'][$selekcja['Tables_in_Selekcje']] = array('query'=>"SELECT email FROM `Selekcje`.`".$selekcja['Tables_in_Selekcje']."`");
}
$others = $sql->FetchAll("SHOW TABLES FROM Others");
foreach ($others as $other){
	$queries['Inne'][$other['Tables_in_Others']] = array('query'=>"SELECT email FROM `Others`.`".$other['Tables_in_Others']."`");
}


$help_var = 0;
$level = 0;
$arr_variable = array();
function print_arr($arr, $name_prefix){
	global $help_var, $level, $arr_variable;
	echo "<ul class='good-list' style='margin-left:".($level*20)."px'>";
	foreach ($arr as $key=>$row){
		if(is_array($row) && !array_key_exists('query', $row)){
			echo "<li class='show-list'>$key</li>";
			$level++;
			print_arr($row, $name_prefix);
			
		}
		else{
			$class="";
			if (isset($row['variables'])){
				foreach ($row['variables'] as $k=>$var){
					$class = "show-input";
					echo "<input type='hidden' name='$name_prefix".'['."$help_var][var][$k][var_name]' value='".$var['name']."'>" ;
					$arr_variable[$name_prefix][] = "<input type='text' name='$name_prefix".'['."$help_var][var][$k][var_val]' id='variable_".$k."_id_$help_var' var_for = 'id_number_$help_var'>
					<label for='variable_".$k."_id_$help_var'>".$var['show_name']."</label>";
				}
			}

			echo "<li><input type='checkbox' value=".'"'.$row['query'].'"'." id='id_number_$help_var' name='$name_prefix".'['."$help_var][value]' class='$class'>
			<label for='id_number_".$help_var++."'>$key</label>";
			$level = 0;
			
		}
	}
	echo "</ul>";
}

function print_query($full_q, &$query_count){
	$query_count++;
	echo "<div class='code col m10'>
	<span>$query_count</span>	
	<pre>$full_q</pre>
	</div>";
}

function print_error($error){
	echo "<div class='code col m10'>
	<pre class='warning'>$error</pre>
	</div>";
}


function do_query($query, &$query_count, $done_str="Affected %n% rows"){
	global $sql, $_POST, $all_queries, $date;
	if (isset($_POST['is_planned'])){
		print_query($query, $query_count);
		$all_queries[] = urlencode($query);
		echo "<div class='col m2 code'><pre>$date</pre></div>";
		$rows_count = 0;
	}
	else{
		print_query($query, $query_count);
		$result = $sql->query($query);
		if (!$result){
			print_error($result);
			exit;
		}
		$rows_count = $sql->getAffectedRows();
		$done_str = str_replace("%n%", $rows_count, $done_str);
		$done_str = "<div class='col m2 code'><pre>$done_str</pre></div>";
		echo $done_str;
	}
	return $rows_count;
}


function get_var_name($key, $i){
	$my_arr = array();
	$offset = 0;
	for ($pos = 0; $pos<strlen($key); $pos++){
		$find = strpos($key, '$', $offset);
		if ($find === false)
			break;
		$offset = $find+1;
		$tmp = "";
		for ($j = $find; isset($key[$j]) && $key[$j] != ' '; $j--){
			$tmp = $key[$j].$tmp;
		}
		$my_arr[] = $tmp;
	}
	return $my_arr[$i];
}

?>
