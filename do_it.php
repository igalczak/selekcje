<?php  require 'config.php';?>
<html>
<head>
<title>&hearts;&hearts;&hearts; SELEKCJE &hearts;&hearts;&hearts;</title>
<meta charset="utf-8">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="materialize.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="materialize.js"></script>
<script src="super_alert.js"></script>
</head>
<body>
	<div class="container" style="min-width: 85%">
		<div class="row">
			<div class="col m12">
<?php

global $sql, $adresy_kontrolne;

if ($_SERVER['REQUEST_METHOD'] == "POST"){
	$time_for_it = time();
	$query_count = 0;
	$act = '`Selekcje`.`'.$_POST['act'].'`';
	$act_dolos = '`Selekcje`.`'.$_POST['act'].'-dolos`';
	$all_queries = array();
	$date = $_POST['date_plan'].' '.$_POST['time_plan'];
	
	$query = "CREATE TABLE IF NOT EXISTS $act_dolos (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`email` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
	
	$sql->query($query);
	
	$query = "CREATE TABLE IF NOT EXISTS $act (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`email` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci";
	
	$sql->query($query);
	$query = "TRUNCATE $act_dolos";
	$sql->query($query);
	$query = "TRUNCATE $act";
	$sql->query($query);
	
	//wybór grupy
	foreach ($_POST['select'] as $row){
		if (isset ($row['value'])){
			$query = "INSERT INTO $act_dolos (`email`)\n\t";
			$q = $row['value'];
			if (isset($row['var'])){
				foreach ($row['var'] as $var){
					$q = str_replace($var['var_name'], $var['var_val'], $q);
				}
			}
			$query .= $q;
			do_query($query, $query_count, "Insert %n% rows");
		}
	}

	//Deduplikacja wewnetrzna
	$query = "INSERT INTO $act (`email`) \n\tSELECT email FROM $act_dolos GROUP BY `email`";
	do_query($query, $query_count, "Insert %n% rows");

	//Usuniecie dolosa
	$query = "DROP TABLE $act_dolos";
	do_query($query, $query_count, "Dolos droped");
	
	//stare na nowe
	$query = "UPDATE $act
	INNER JOIN `Robinson`.`rob-emails-substitution` ON $act.`email` = `Robinson`.`rob-emails-substitution`.`old-email`
	SET $act.`email` = `Robinson`.`rob-emails-substitution`.`new-email`";
	do_query($query, $query_count, "Update %n% rows");
	
	//Deduplikacja z robinsonem
	$query = "DELETE FROM $act WHERE email IN (SELECT * FROM (
	SELECT `Robinson`.`hard-bounces`.`email` AS email FROM `Robinson`.`hard-bounces`
	UNION ALL
	SELECT `Robinson`.`rob-emails`.`email` AS email FROM `Robinson`.`rob-emails`
	UNION ALL
	SELECT `Robinson`.`rob-author-emails`.`email` AS email FROM `Robinson`.`rob-author-emails`
	) AS p \n)";
	do_query($query, $query_count, "Deleted %n% rows");

	/*
	$query = "UPDATE $act
	INNER JOIN `Robinson`.`rob-emails-substitution` ON $act.`email` = `Robinson`.`rob-emails-substitution`.`old-email`
	SET $act.`email` = `Robinson`.`rob-emails-substitution`.`new-email`";
	print_query($query, $query_count);
	if (!$sql->query($query)){
		print_error($sql->getError());
		exit;
	}
	$rows_count = $sql->getAffectedRows();
	echo "<div class='col m2 code'><pre>Update $rows_count rows</pre></div>"
	*/
	
	//Deduplikacja z grupą wykluczenia
	if (isset($_POST['delete'])){
		foreach ($_POST['delete'] as $row){
			if (isset ($row['value'])){
				$query = "DELETE FROM $act WHERE email IN (\n\tSELECT * FROM (\n\t";
				$q = $row['value'];
				if (isset($row['var'])){
					foreach ($row['var'] as $var){
						$q = str_replace($var['var_name'], $var['var_val'], $q);
					}
				}
				$query = $query.$q." \n\t) AS p\n )";
				do_query($query, $query_count, "Deleted %n% rows");
			}
		}
	}
	$query = "DELETE FROM $act WHERE email='' OR email=' ' OR email IS NULL";
	do_query($query, $query_count, "Deleted %n% rows");
	
	//Dołączanie adresów kontrolnych
	$query = "INSERT INTO $act (`email`) VALUES ";
	foreach ($adresy_kontrolne as $email)
		$query .= "\n('$email'),";
	$query[strlen($query)-1] = '';
	do_query($query, $query_count, "Insert %n% rows");
	
	$query = "SELECT * FROM $act";
	do_query($query, $query_count, "All %n% rows");
	
	$query = "SELECT * FROM $act WHERE
	$act.`email` Not Like '%@%.%' OR
	$act.`email` REGEXP 'ą|ę|ć|ł|ń|ó|ź|ż|ś' OR
	$act.`email` LIKE '%[%' OR $act.`email` LIKE '%]%' OR $act.`email` LIKE '%(%' OR $act.`email` LIKE '%)%' OR 
	$act.`email` LIKE '%:%' OR $act.`email` LIKE '%;%' OR $act.`email` LIKE '%,%' OR $act.`email` LIKE '%`%' OR 
	$act.`email` LIKE '%#%' OR $act.`email` LIKE '%$%' OR $act.`email` LIKE '%^%' OR 
	$act.`email` LIKE '%&%' OR $act.`email` LIKE '%*%' OR $act.`email` Like '@%' OR
	$act.`email` Like '% %' OR $act.`email` Like '%.' OR
	$act.`email` Like '%@' OR $act.`email` Like '%@%@%'";
	$rows_count = do_query($query, $query_count, "Wrong %n% rows");
	$time_for_it = time() - $time_for_it;
	echo "<div class='col m10 center-align'><p>I did IT in ".((int)($time_for_it/60)==0?$time_for_it." seconds":(int)($time_for_it/60)." minutes")."</p></div>";
	if ($rows_count > 0)
		echo "<div class='col m5 center-align'><a href='correct_email.php?table=$act' class='btn' target='_blanc'>Popraw</a></div>";
	echo "<div class='col m5 center-align'><a href='index.php' class='btn'>Nowa selekcja</a></div>";
	
	if (isset($_POST['is_planned'])){
		$insert = array(
				'do_it_date'=>$date, 
				'queries'=>implode(';',$all_queries), 
				'isActive'=>1, 
				'isDone'=>0,
				'act'=>$_POST['act'],
		);
		$sql->SelectDb('Selekcje');
		$sql->Insert('_tasks', $insert);
	}
	else{
		$export = $sql->FetchAll("SELECT * FROM $act");
		if (!$export)
			die ($sql->getError());
		$keys = array_keys($export[0]);
		
		$output = implode(';',$keys)."\n";
		foreach ($export as $row){
			$output .= implode(';',$row)."\n";
		}
		$file = "/var/www/html/auto-selekcje/Selekcje/".$_POST['act'].".txt";
		file_put_contents($file, $output);
		chmod($file, 0666);
		
		//Copy to Merketing/Akcje/xxx
		$command = "bash /var/www/html/auto-selekcje/copy ".$_POST['act'];
		$output = 0;
		exec($command, $output);
		echo implode("<br>",$output);
	}
}
else
	echo "<script>location.href='index.php'</script>";

?>
</div>
</div>
</div>
</body>
</html>