function super_alert(html, type){
	var ico = "";
	switch (type){
	case 'error': ico = "warning"; break;
	case 'success': ico="done_all"; break;
	case 'info': ico="info_outline"; break;
	}
	var app ='';
    app += '<div id="modal-alert" class="modal alert-'+type+'">';
    app += '<i class="material-icons">'+ico+'</i>';
    app += '<p class="center-align">'+html+'</p>';
    app += '</div>';
    $("body").append(app);
}