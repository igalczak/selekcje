<?php  require 'config.php'; ?>
<html>
<head>
<title>&hearts;&hearts;&hearts; SELEKCJE &hearts;&hearts;&hearts;</title>
<meta charset="utf-8">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="materialize.css">
<link rel="stylesheet" href="font-awesome/css/font-awesome.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="materialize.js"></script>
<script src="super_alert.js"></script>
</head>
<body>
	<div class="container">
		<form action='do_it.php' method='post' class='row' id="selectin_form">
			<div class="col m12">
				<div class="col m6">
					<div class="card hoverable">
						<div class="card-content">
							<span class="card-title">Wybierz grupę</span>
							<?php  print_arr($queries, "select"); ?>
						</div>
					</div>
				</div>
				<div class="col m6">
					<div class="card hoverable">
						<div class="card-content">
							<span class="card-title">Wyklucz z ...</span>
							<?php  print_arr($queries, "delete"); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col m6 center-align input-field">
			<?php foreach ($arr_variable['select'] as $input){
				echo "<div class='col m12 variable'>$input</div>";
			}?>
			</div>
			<div class="col m6 center-align input-field">
			<?php foreach ($arr_variable['delete'] as $input){
				echo "<div class='col m12 variable'>$input</div>";
			}?>
			</div>
			<div class="col m6 center-align input-field">
				<input type="checkbox" class="validate" name="is_planned" id="is_planned">
				<label for="is_planned">Zaplanuj zadanie</label>
			</div>
			<div class="col m3 center-align input-field">
				<input type="text" class="datepicker" name="date_plan" id="date_plan" disabled>
				<label for="date_plan">Data zadania</label>
			</div>
			<div class="col m3 center-align input-field">
				<input type="text" class="validate" name="time_plan" id="time_plan" disabled> 
				<label for="time_plan">Czas: HH:MM:SS</label>
			</div>
			<div class="col m12 center-align input-field">
				<input type="text" class="validate" name="act" id="act" required>
				<label for="act">Numer akcji</label>
			</div>
			<div class="col m12 center-align">
				<button type='submit' class='btn btn-submit'>Zatwierdź</button>
			</div>
		</form>
	</div>
<script>
$(".show-input").prop('checked',false);
$(".show-input").change(function(){
	var inputs = [];
	var show_it = $(this).is(":checked");
	var search = $(this).attr('id');
	$(".variable input").each(function(){
		if ($(this).attr('var_for') == search){
			if (show_it){
				$(this).parent().show();
				$(this).attr('required', 'required');
			}
			else{
				$(this).parent().hide();
				$(this).removeAttr('required');
			}
		}
	});
});

$(".good-list").find(".good-list").hide();
$(".show-list").prepend('<i class="fa fa-chevron-right" style="margin-right:10px"></i>');

$(".show-list").click(function(event){
	if ($(this).children("i").hasClass("fa-chevron-right")){
		$(this).children("i").removeClass("fa-chevron-right").addClass("fa-chevron-down");
		$(this).next('ul').eq(0).show();
	}
	else{
		$(this).children("i").removeClass("fa-chevron-down").addClass("fa-chevron-right");
		$(this).next('ul').eq(0).hide();
	}
	event.preventDefault();
});

$('.datepicker').pickadate({
	dateFormat: "yyyy-mm-dd",
	selectYears: 10,
	monthsFull: [ "stycznia", "lutego", "marca", "kwietnia", "maja", "czerwca", "lipca", "sierpnia", "września", "października", "listopada", "grudnia" ],
	format: 'd mmmm, yyyy r.',
	formatSubmit: 'yyyy-mm-dd',
	hiddenName: true
})

if (!$("#is_planned").is("checked")){
	$("#date_plan").attr("disabled","disabled");
	$("#time_plan").attr("disabled","disabled");
}
else{
	$("#date_plan").removeAttr("disabled");
	$("#time_plan").removeAttr("disabled");
}
$("#is_planned").prop('checked',false);
$("#is_planned").change(function(){
	if ($(this).is(":checked")){
		$("#date_plan").removeAttr("disabled");
		$("#time_plan").removeAttr("disabled");
	}
	else{
		$("#date_plan").attr("disabled","disabled");
		$("#time_plan").attr("disabled","disabled");
	}

});

</script>
</body>
</html>