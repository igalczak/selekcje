<?php  require 'config.php';?>
<html>
<head>
<title>&hearts;&hearts;&hearts; CHECK &hearts;&hearts;&hearts;</title>
<meta charset="utf-8">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="materialize.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="materialize.js"></script>
<script src="super_alert.js"></script>
</head>
<body>
	<div class="container" style="min-width: 85%">
		<div class="row">
			<div class="col m12" id="table-result">
<?php
if (!isset($_GET['table']))
	exit;

global $sql;
	
$table = $_GET['table'];
$query = "SELECT 1 FROM $table LIMIT 1";
if (!$sql->query($query)){
	print_error($sql->getError());
	exit;
}

$query = "SELECT * FROM $table WHERE
$table.`email` Not Like '%@%.%' OR
$table.`email` REGEXP 'ą|ę|ć|ł|ń|ó|ź|ż|ś' OR
$table.`email` LIKE '%[%' OR $table.`email` LIKE '%]%' OR $table.`email` LIKE '%(%' OR $table.`email` LIKE '%)%' OR
$table.`email` LIKE '%:%' OR $table.`email` LIKE '%;%' OR $table.`email` LIKE '%,%' OR $table.`email` LIKE '%`%' OR
$table.`email` LIKE '%#%' OR $table.`email` LIKE '%$%' OR $table.`email` LIKE '%^%' OR
$table.`email` LIKE '%&%' OR $table.`email` LIKE '%*%' OR $table.`email` Like '@%' OR
$table.`email` Like '% %' OR $table.`email` Like '%.' OR
$table.`email` Like '%@' OR $table.`email` Like '%@%@%'";

$query = str_replace("\n", " ", $query);


echo '<script>var query = "'.$query.'";</script>';
echo '<script>var table = "'.$table.'";</script>';
?>
</div>
<div class="col m12 center-align" id="div_save_changes">
	
</div>



<script>
$("ducument").ready(refresh_table(query));
$("#save_changes").click(function(){
	var update_arr = new Array();
	$("input").each(function(){
		update_arr.push($(this).attr('row_id'), $(this).attr('field'), $(this).val());
	});
	var update_query = new Array();
	for (var i=0; i<update_arr.length; i++){
		update_query.push("UPDATE "+table+" SET `"+update_arr[i+1]+"`='"+update_arr[i+2]+"' WHERE id='"+update_arr[i]+"'");
		i += 2;
	}
	var result = true;
	for (var i=0; i<update_query.length; i++){
	    $.ajax({
	    	url: "do_query.php",
	    	type: "POST",
	    	data: {query: update_query[i]},
	    	async: false,
	    }).done(function(msg){
	    	result &= msg;
	   	});

	}
	/*
	if (result)
		alert ("udało się");
	*/
	refresh_table(query);
});


function refresh_table(query){
	$("#table-result").addClass('loading');
	$("#loader").show();
    $.ajax({
    	url: "get_select_result.php",
    	type: "POST",
    	data: {query: query},
    	async: false,
    }).done(function(msg){
    	$("#table-result").html(msg);
    	$("#loader").hide();
    	$("#table-result").removeClass('loading');
    	if (!$("#empty").length)
    		$("#div_save_changes").html('<button id="save_changes" class="btn">Zapisz zmiany</button>');
   	});
}





</script>

</div>
</div>
</body>
</html>